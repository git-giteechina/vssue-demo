module.exports = {
  title: 'Vssue Demo',
  description: 'A Vue-powered Issue-based Comment Plugin',

  plugins: [
    ['@vssue/vuepress-plugin-vssue', {
      platform: 'gitee',
      owner: 'git_giteechina',
      repo: 'vssue-demo',
      clientId: '98908017cf9f4270a6e0ed5019bb0badf452caa3e9ec481ee852f81519cbd0b8',
      clientSecret: '8b9c8e7e90c775077e8c77f39166f48af86f8da6fdfacba05dfb12e45b37448d',
    }],
  ],
}
